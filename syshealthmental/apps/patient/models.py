# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from syshealthmental.apps.patient.validators import validate_rg, validate_cpf, validate_phone, validate_cep

# Create your models here.


class Patient(models.Model):

    GENDER_CHOICES = (
        ('F', 'Feminino'),
        ('M', 'Masculino')
    )
    CIVIL_STATUS_CHOICES = (
        ('S', 'Solteiro (a)'),
        ('C', 'Casado (a)'),
        ('SE', 'Separado (a)'),
        ('D', 'Divorciado (a)'),
        ('V', 'Viúvo (a)'),
    )
    SCHOLARITY_CHOICES = (
        ('FI', 'Fundamental Incompleto'),
        ('FC', 'Fundamental Completo'),
        ('MI', 'Médio Incompleto'),
        ('MC', 'Médio Completo'),
        ('SI', 'Superior Incompleto'),
        ('SC', 'Superior Completo'),
        ('PGI', 'Pós-graduação Incompleto'),
        ('PGC', 'Pós-graduação Completo'),
        ('MEI', 'Mestrado Incompleto'),
        ('MEC', 'Mestrado Completo'),
        ('DOI', 'Doutorado Incompleto'),
        ('DOC', 'Doutorado Completo'),
    )
    FAMILY_INCOME_CHOICES = (
        ('SR', 'Não possui renda'),
        ('MU', 'Meio até 1 salário mínimo'),
        ('DT', '2 até 3 salários mínimos'),
        ('QC', '4 até 5 salários mínimos'),
        ('CM', '5 salários mínimos ou mais'),
    )
    name = models.CharField(
        u'Nome Completo', max_length=255
    )
    nickname = models.CharField(
        u'Apelido', max_length=25, blank=True
    )
    date_of_birth = models.DateField(
        u'Data de nascimento'
    )
    age = models.CharField(
        u'Idade', max_length=3
    )
    gender = models.CharField(
        u'Sexo', max_length=1, choices=GENDER_CHOICES
    )
    mother_name = models.CharField(
        u'Mãe', max_length=255
    )
    father_name = models.CharField(
        u'Pai', max_length=255, blank=True
    )
    cns = models.CharField(
        u'Cartão Nacional de Saúde', max_length=255, unique=True
    )
    cpf = models.CharField(
        u'CPF', max_length=11, unique=True, validators=[validate_cpf]
    )
    rg = models.CharField(
        u'Registro Geral', max_length=255, validators=[validate_rg]
    )
    date_of_expedition = models.DateField(
        u'Data de Expedição'
    )
    nationality = models.CharField(
        u'Nacionalidade', max_length=255, blank=True
    )
    civil_status = models.CharField(
        u'Estado Civil', max_length=2, choices=CIVIL_STATUS_CHOICES, blank=True
    )
    children = models.CharField(
        u'Filhos', max_length=3, blank=True
    )
    scholarity = models.CharField(
        u'Escolaridade', max_length=3, choices=SCHOLARITY_CHOICES, blank=True
    )
    ethnicity = models.CharField(
        u'Etnia', max_length=255, blank=True
    )
    phone = models.CharField(
        u'Telefone', max_length=11, validators=[validate_phone]
    )
    live_with = models.CharField(
        u'Reside com', max_length=255, blank=True
    )
    address = models.CharField(
        u'Endereço', max_length=255, blank=True
    )
    cep = models.CharField(
        u'CEP', max_length=8, validators=[validate_cep], blank=True
    )
    landmark = models.CharField(
        u'Ponto de Referência', max_length=255, blank=True
    )
    workplace = models.CharField(
        u'Local de trabalho', max_length=255, blank=True
    )
    profession = models.CharField(
        u'Profissão', max_length=100, blank=True
    )
    family_income = models.CharField(
        u'Renda', max_length=2, choices=FAMILY_INCOME_CHOICES, blank=True
    )

    @property
    def full_name(self):
        return self.name

    @property
    def short_name(self):
        name = self.name.split(' ')
        if len(name) > 1:
            return '%s %s' % (name[0], name[1])
        else:
            return name[0]

    @property
    def first_name(self):
        return '%s' % self.name.split(' ')[0]

    def __unicode__(self):
        return u'Paciente %s - %s' % (self.cpf, self.name)
