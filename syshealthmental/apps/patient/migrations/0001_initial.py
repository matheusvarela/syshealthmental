# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-10-19 17:00
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models
import syshealthmental.apps.patient.validators


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Patient',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Nome Completo')),
                ('nickname', models.CharField(blank=True, max_length=25, verbose_name='Apelido')),
                ('date_of_birth', models.DateField(verbose_name='Data de nascimento')),
                ('age', models.CharField(max_length=3, verbose_name='Idade')),
                ('gender', models.CharField(choices=[('F', 'Feminino'), ('M', 'Masculino')], max_length=1, verbose_name='Sexo')),
                ('mother_name', models.CharField(max_length=255, verbose_name='M\xe3e')),
                ('father_name', models.CharField(blank=True, max_length=255, verbose_name='Pai')),
                ('cns', models.CharField(max_length=255, unique=True, verbose_name='Cart\xe3o Nacional de Sa\xfade')),
                ('cpf', models.CharField(max_length=11, unique=True, validators=[syshealthmental.apps.patient.validators.validate_cpf], verbose_name='CPF')),
                ('rg', models.CharField(max_length=255, validators=[django.core.validators.RegexValidator(b'(^\\d+(\\.\\d+)*[\\/]+\\w*[-]+\\w+$)|(^\\d+(\\.\\d+)*$)', message='O RG \xe9 inv\xe1lido.')], verbose_name='Registro Geral')),
                ('date_of_expedition', models.DateField(verbose_name='Data de Expedi\xe7\xe3o')),
                ('nationality', models.CharField(blank=True, max_length=255, verbose_name='Nacionalidade')),
                ('civil_status', models.CharField(blank=True, choices=[('S', 'Solteiro (a)'), ('C', 'Casado (a)'), ('SE', 'Separado (a)'), ('D', 'Divorciado (a)'), ('V', 'Vi\xfavo (a)')], max_length=2, verbose_name='Estado Civil')),
                ('children', models.CharField(blank=True, max_length=3, verbose_name='Filhos')),
                ('scholarity', models.CharField(blank=True, choices=[('FI', 'Fundamental Incompleto'), ('FC', 'Fundamental Completo'), ('MI', 'M\xe9dio Incompleto'), ('MC', 'M\xe9dio Completo'), ('SI', 'Superior Incompleto'), ('SC', 'Superior Completo'), ('PGI', 'P\xf3s-gradua\xe7\xe3o Incompleto'), ('PGC', 'P\xf3s-gradua\xe7\xe3o Completo'), ('MEI', 'Mestrado Incompleto'), ('MEC', 'Mestrado Completo'), ('DOI', 'Doutorado Incompleto'), ('DOC', 'Doutorado Completo')], max_length=3, verbose_name='Escolaridade')),
                ('ethnicity', models.CharField(blank=True, max_length=255, verbose_name='Etnia')),
                ('phone', models.CharField(max_length=11, validators=[syshealthmental.apps.patient.validators.validate_phone], verbose_name='Telefone')),
            ],
        ),
    ]
