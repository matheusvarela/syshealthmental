# -*- coding:utf-8 -*-
from django import template

register = template.Library()

@register.filter(name="class")
def add_class(value, arg):
    return value.as_widget(attrs={"class": arg})

@register.filter(name='mask_cpf')
def mask_cpf(value):
    if value:
        return value[:3] + '.' + value[3:6] + '.' + value[6:9] + '-' + value[9:]
    return value

@register.filter(name='mask_cep')
def mask_cep(value):
    if value:
        return value[:5] + '-' + value[5:]
    return value

@register.filter(name='mask_phone')
def mask_phone(value):
    if value and len(value) == 11:
        return '(' + value[:2] + ')' + ' ' + value[2:7] + '-' + value[7:]
    elif value and len(value) == 10:
        return '(' + value[:2] + ')' + ' ' + value[2:6] + '-' + value[6:]
    return value
