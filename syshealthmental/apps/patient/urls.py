# -*- coding: utf-8 -*-

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^list', views.patient_list, name='patient_list'),
    url(r'^(?P<pk>\d+)/detail$', views.patient_detail, name='patient_detail'),
    url(r'^new', views.patient_new, name='patient_new'),
    url(r'^(?P<pk>\d+)/edit$', views.patient_edit, name='patient_edit'),
]
