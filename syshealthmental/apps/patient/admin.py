# -*- coding: utf-8 -*-
from django.contrib import admin

from syshealthmental.apps.patient.models import Patient

# Register your models here.

admin.site.register(Patient)
