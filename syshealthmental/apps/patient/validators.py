# -*- coding: utf-8 -*-

from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
import re

def only_digits(value):
    """Remove tudo o que não é dígito"""
    return re.sub('\D', '', value)

validate_rg = RegexValidator(r'(^\d+(\.\d+)*[\/]+\w*[-]+\w+$)|(^\d+(\.\d+)*$)',
                              message=u'O RG é inválido.')

def validate_cpf(value):

    value = only_digits(value)

    if len(value) == 11:

        cpf_invalid = [11*str(i) for i in range(10)]

        if not all(c.isdigit() for c in value) or value in cpf_invalid:
            raise ValidationError(u'O CPF é inválido.')

        clean = [int(c) for c in value]
        cpf = clean[:9]

        while len(cpf) < 11:
            r = sum([(len(cpf)+1-i)*v for i, v in
                    [(x, cpf[x]) for x in range(len(cpf))]]) % 11

            if r > 1:
                f = 11 - r
            else:
                f = 0
            cpf.append(f)

        if cpf != clean:
            raise ValidationError(u'O CPF é inválido.')
    else:
        raise ValidationError(u'O CPF é inválido.')


def validate_phone(value):

    phone = only_digits(value)

    if len(phone) == 11:

        phone_invalid = [11*str(i) for i in range(10)]

        if not all(p.isdigit() for p in phone) or phone in phone_invalid:
            raise ValidationError(u'Telefone inválido.')
        return phone

def validate_cep(value):

    cep = only_digits(value)

    if len(cep) == 8:

        cep_invalid = [8*str(i) for i in range(7)]

        if not all(c.isdigit() for c in cep) or cep in cep_invalid:
            raise ValidationError(u'CEP inválido.')
        return cep
    else:
        raise ValidationError(u'O CEP é inválido.')
