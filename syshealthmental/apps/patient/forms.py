# -*- coding:utf-8 -*-

from django import forms
from syshealthmental.apps.patient.validators import validate_cpf, only_digits, validate_phone, validate_cep
from syshealthmental.apps.patient.models import Patient
from localflavor.br.forms import BRPhoneNumberField

class PatientForm(forms.ModelForm):

    phone = BRPhoneNumberField(
        error_messages={'invalid': ('Número inválido.'
                                    'Formatos permitidos: XX-XXXX-XXXX'
                                    ' ou XX-XXXXX-XXXX')},
    )

    cpf = forms.CharField(max_length=14)
    phone = forms.CharField(max_length=15)
    cep = forms.CharField(max_length=9)


    def clean_cpf(self):
        cpf = only_digits(self.cleaned_data['cpf'])
        validate_cpf(cpf)
        if Patient.objects.filter(cpf=cpf).exists():
            instance_cpf_exists = Patient.objects.get(cpf=cpf)

            instance = getattr(self, 'instance', None)

            if instance == instance_cpf_exists:
                pass
            else:
                raise forms.ValidationError(u'Esse CPF já pertence a outro paciente')
        return cpf

    def clean_phone(self):
        phone = only_digits(self.cleaned_data['phone'])

        return validate_phone(phone)

    def clean_cep(self):

        cep = only_digits(self.cleaned_data['cep'])

        return validate_cep(cep)

    class Meta:
        model = Patient
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs = {'required': True}),
            'date_of_birth': forms.DateInput(format=('%d/%m/%Y'), attrs = {'required': True}),
            'mother_name': forms.TextInput(attrs = {'required': True}),
            'cns': forms.TextInput(attrs = {'required': True}),
            'cpf': forms.TextInput(attrs = {'required': True}),
            'rg': forms.TextInput(attrs = {'required': True}),
            'date_of_expedition': forms.DateInput(format=('%d/%m/%Y'), attrs = {'required': True}),
            'age': forms.TextInput(attrs = {'required': True}),
            'phone': forms.TextInput(attrs = {'required': True}),
            'cep': forms.TextInput(attrs = {'required': True}),

        }
