# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class PatientConfig(AppConfig):
    name = 'syshealthmental.apps.patient'
    verbose_name = 'Patient'
